import 'character.dart';

final characters = [
  Character(
    name: 'Jefferson Vera',// cambiar esto
    age: 21,
    image: 'images/jefferson.png', // cambiar de nombre descargarla de nuevas
    jobTitle: 'Flutter Developer',
    stars: 4.3,
  ),
  Character(
    name: 'Dayanna Villamar',
    age: 22,
    image: 'images/dayanna.png',
    jobTitle: 'Android Developer',
    stars: 3.8,
  ),
  Character(
    name: 'Joselin Pico',
    age: 31,
    image: 'images/joselin.png',
    jobTitle: 'iOS Developer',
    stars: 4.9,
  ),
  Character(
    name: 'Anchundia Moises',
    age: 29,
    image: 'images/moises.png',
    jobTitle: 'React Native Developer',
    stars: 4.1,
  ),
  Character(
    name: 'Fredy Macias',
    age: 34,
    image: 'images/freddy.png',
    jobTitle: 'Web Developer',
    stars: 3.5,
  ),
  Character(
    name: 'Josselyn Vera',
    age: 39,
    image: 'images/dayanna.png',
    jobTitle: 'UI Designer',
    stars: 2.9,
  ),
  Character(
    name: 'Victor Lopez',
    age: 31,
    image: 'images/victor.png',
    jobTitle: 'Backend Developer',
    stars: 3.8,
  ),
  Character(
    name: 'Patricio Basurto',
    age: 32,
    image: 'images/patricio.png',
    jobTitle: 'Project Manager',
    stars: 4.6,
  ),
  Character(
    name: 'Carolane Buzella',
    age: 25,
    image: 'images/carolane.png',
    jobTitle: 'QA Team Lead',
    stars: 4.0,
  ),
  Character(
    name: 'Samanta Parraga',
    age: 23,
    image: 'images/samanta.png',
    jobTitle: 'DevOps Team Lead',
    stars: 3.9,
  ),
];
