import 'package:flutter/material.dart';
import '../model/character.dart';

class CharacterWidget extends StatelessWidget {
  final Character character;

  const CharacterWidget({Key? key, required this.character}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(bottom: 15.0),
      decoration: const BoxDecoration(
        color: Color.fromARGB(71, 140, 0, 255),// fondo cambair
        borderRadius: BorderRadius.all(Radius.circular(20.0)),
      ),
      child: Row(
        children: [
          Container(
            padding: const EdgeInsets.all(15.0),
            child: Image.asset(
              character.image,
              height: 160.0, // ambiar por si acaso esta muy grande
            ),
          ),
          Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  margin: const EdgeInsets.only(bottom: 10.0),
                  child: Text(
                    character.name, // aqui va los nombre de las lista 
                    style: const TextStyle(
                      fontSize: 25.0,
                      fontWeight: FontWeight.bold, 
                      color: Color.fromARGB(192, 25, 212, 156),
                    ),
                  ),
                ),
                Row(
                  children: [
                    Container(
                      padding: const EdgeInsets.all(10.0),
                      decoration: BoxDecoration(
                        color: character.stars < 4.0
                            ? Color.fromARGB(136, 170, 24, 121)
                            : Color.fromARGB(155, 214, 61, 14),
                        shape: BoxShape.circle,
                      ),
                      child: Text(
                        character.stars.toString(),
                        style: const TextStyle(
                            fontSize: 18.0, color: Color.fromARGB(255, 0, 0, 0)),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(left: 8.0),
                      child: Text(
                        character.jobTitle,
                        style: const TextStyle(
                          fontSize: 17.0,
                        ),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
